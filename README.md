# SPReg (Perun) -> MitreID sync

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2024)

This project has reached end of life, which means no new features will be added. Security patches and important bug fixes will end as of 2024. Check out [Federation registry](https://github.com/rciam/rciam-federation-registry) and its mitreID deployment agent instead.

## Description

Command-line tool for synchronization of data stored about the clients in Perun to MitreID.
Serves as one-shot synchronization. To run it periodically, we advise to create a cron job.

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).
Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

### Available scopes

- script

## Configuration

See [example configuration](src/main/resources/application.yml).

## Build

First, package the application. If you have already downloaded the compiled JAR file, skip to the next step.

```bash
./mvnw clean package -Dfinal.name=oidc-sync-v1
```

### Arguments

- final.name: name of the built jar

Run the JAR with options to specify config file location and name (name defaults to application.yml)

```bash
java -jar PATH/TO/RUNNABLE/JAR/FILE.jar --spring.config.location=/PATH/TO/DIR/WITH/CONFIG/
 --spring.config.name=FILE_NAME_WITHOUT_EXTENSION --mode=to_perun --interactive=true
```

## Run

### Arguments

- MODE: specifies the destination to which the sync will be performed
  - to_perun
  - to_oidc
- INTERACTIVE: if set to TRUE, each action has to be confirmed by the user
  - true
  - false
