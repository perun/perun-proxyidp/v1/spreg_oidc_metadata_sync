package cz.muni.ics.oidc.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

public enum CredentialTimeoutTimeUnit {
    SECONDS('s'),
    MINUTES('m'),
    HOURS('h'),
    DAYS('d'),
    WEEKS('w'),
    YEARS('y');

    @Getter
    private final char unit;

    CredentialTimeoutTimeUnit(char unit) {
        this.unit = unit;
    }

    private static final Map<Character, CredentialTimeoutTimeUnit> lookup = new HashMap<>();

    static {
        for (CredentialTimeoutTimeUnit unit: CredentialTimeoutTimeUnit.values()) {
            lookup.put(unit.unit, unit);
        }
    }

    /**
     * Resolve unit by unit char.
     * @param unit character signalizing the time unit
     * @return Resolved unit or SECONDS as default in case of invalid
     * character provided (incl. null).
     */
    public static CredentialTimeoutTimeUnit resolve(Character unit) {
        if (unit == null) {
            return CredentialTimeoutTimeUnit.SECONDS;
        }
        return lookup.getOrDefault(unit, SECONDS);
    }

}
