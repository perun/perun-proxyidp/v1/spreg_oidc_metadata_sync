package cz.muni.ics.oidc;

import cz.muni.ics.oidc.data.ClientRepository;
import cz.muni.ics.oidc.exception.InvalidPropertyException;
import cz.muni.ics.oidc.exception.PerunConnectionException;
import cz.muni.ics.oidc.exception.PerunUnknownException;
import cz.muni.ics.oidc.models.FacilityWithAttributes;
import cz.muni.ics.oidc.models.MitreidClient;
import cz.muni.ics.oidc.models.PKCEAlgorithm;
import cz.muni.ics.oidc.models.PerunAttributeValue;
import cz.muni.ics.oidc.models.PerunAttributeValueAwareModel;
import cz.muni.ics.oidc.models.SyncResult;
import cz.muni.ics.oidc.props.ActionsProperties;
import cz.muni.ics.oidc.props.AttrsMapping;
import cz.muni.ics.oidc.props.ConfProperties;
import cz.muni.ics.oidc.props.CredentialTimeout;
import cz.muni.ics.oidc.props.GrantTypesTimeoutsProperties;
import cz.muni.ics.oidc.rpc.PerunAdapter;
import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static cz.muni.ics.oidc.Synchronizer.DO_YOU_WANT_TO_PROCEED;
import static cz.muni.ics.oidc.Synchronizer.SPACER;
import static cz.muni.ics.oidc.Synchronizer.Y;
import static cz.muni.ics.oidc.Synchronizer.YES;

@Component
@Slf4j
public class ToOidcSynchronizer {

    public static final String OFFLINE_ACCESS = "offline_access";

    // flow types
    public static final String AUTHORIZATION_CODE = "authorization code";
    public static final String DEVICE = "device";
    public static final String IMPLICIT = "implicit";
    public static final String HYBRID = "hybrid";

    public static final String CLIENT_CREDENTIALS = "client credentials";

    public static final String TOKEN_EXCHANGE = "token exchange";

    // grant types
    public static final String GRANT_AUTHORIZATION_CODE = "authorization_code";
    public static final String GRANT_IMPLICIT = "implicit";
    public static final String GRANT_DEVICE = "urn:ietf:params:oauth:grant-type:device_code";
    public static final String GRANT_HYBRID = "hybrid";
    public static final String GRANT_REFRESH_TOKEN = "refresh_token";
    public static final String GRANT_CLIENT_CREDENTIALS = "client_credentials";
    public static final String GRANT_TOKEN_EXCHANGE = "urn:ietf:params:oauth:grant-type:token-exchange";

    // timeouts
    public static final String ACCESS_TOKEN_TIMEOUT = "access_token";
    public static final String ID_TOKEN_TIMEOUT = "id_token";
    public static final String REFRESH_TOKEN_TIMEOUT = "refresh_token";
    public static final String DEVICE_CODE_TIMEOUT = "device_code";

    // response types
    public static final String RESPONSE_CODE = "code";
    public static final String RESPONSE_TOKEN = "token";
    public static final String RESPONSE_ID_TOKEN = "id_token";
    public static final String RESPONSE_TOKEN_ID_TOKEN = RESPONSE_TOKEN + " " + RESPONSE_ID_TOKEN;
    public static final String RESPONSE_ID_TOKEN_TOKEN = RESPONSE_ID_TOKEN + " " + RESPONSE_TOKEN;
    public static final String RESPONSE_CODE_ID_TOKEN = RESPONSE_CODE + " " + RESPONSE_ID_TOKEN;
    public static final String RESPONSE_CODE_TOKEN = RESPONSE_CODE + " " + RESPONSE_TOKEN;
    public static final String RESPONSE_CODE_TOKEN_ID_TOKEN = RESPONSE_CODE_TOKEN + " " + RESPONSE_ID_TOKEN;
    public static final String RESPONSE_CODE_ID_TOKEN_TOKEN = RESPONSE_CODE_ID_TOKEN + " " + RESPONSE_TOKEN;

    // response types for grant types
    private static final String[] RESPONSE_TYPE_AUTH_CODE = { RESPONSE_CODE };
    private static final String[] RESPONSE_TYPE_IMPLICIT = {
            RESPONSE_ID_TOKEN, RESPONSE_TOKEN, RESPONSE_ID_TOKEN_TOKEN, RESPONSE_TOKEN_ID_TOKEN
    };
    private static final String[] RESPONSE_TYPE_HYBRID = {
            RESPONSE_CODE_TOKEN, RESPONSE_CODE_ID_TOKEN, RESPONSE_CODE_ID_TOKEN,
            RESPONSE_CODE_ID_TOKEN_TOKEN, RESPONSE_CODE_TOKEN_ID_TOKEN
    };

    public static final String PKCE_TYPE_NONE = "none";
    public static final String PKCE_TYPE_PLAIN = "plain code challenge";
    public static final String PKCE_TYPE_SHA256 = "SHA256 code challenge";
    private final PerunAdapter perunAdapter;
    private final String proxyIdentifier;
    private final String proxyIdentifierValue;
    private final AttrsMapping perunAttrNames;
    private final ClientRepository clientRepository;
    private final ActionsProperties actionsProperties;
    private final GrantTypesTimeoutsProperties credentialTimeoutByGrantProps;
    private final Cipher cipher;
    private final SecretKeySpec secretKeySpec;

    private final Scanner scanner = new Scanner(System.in);

    private boolean interactiveMode = false;
    private final boolean proceedToDelete;

    @Autowired
    public ToOidcSynchronizer(@NonNull PerunAdapter perunAdapter,
                              @NonNull ConfProperties confProperties,
                              @NonNull AttrsMapping perunAttrNames,
                              @NonNull ClientRepository clientRepository,
                              @NonNull ActionsProperties actionsProperties,
                              @NonNull GrantTypesTimeoutsProperties credentialTimeoutByGrantProps,
                              @NonNull Cipher cipher,
                              @NonNull SecretKeySpec secretKeySpec)
    {
        this.perunAdapter = perunAdapter;
        this.perunAttrNames = perunAttrNames;
        this.clientRepository = clientRepository;
        this.actionsProperties = actionsProperties;
        this.credentialTimeoutByGrantProps = credentialTimeoutByGrantProps;
        this.cipher = cipher;
        this.secretKeySpec = secretKeySpec;
        this.proxyIdentifier = perunAttrNames.getProxyIdentifier();
        this.proxyIdentifierValue = confProperties.getProxyIdentifierValue();
        proceedToDelete = actionsProperties.getToOidc().isDelete();
    }

    public SyncResult syncToOidc(boolean interactiveMode) {
        this.interactiveMode = interactiveMode;
        log.info("Started synchronization to OIDC DB");
        SyncResult res = new SyncResult();
        Set<FacilityWithAttributes> facilities;
        try {
            facilities = new HashSet<>(perunAdapter.getFacilitiesByAttributeWithAttributes(
                    proxyIdentifier, proxyIdentifierValue, perunAttrNames.getNames()));
        } catch (PerunConnectionException | PerunUnknownException e) {
            log.error("Caught exception when fetching facilities by attr '{}' with value '{}' with attributes '{}'",
                    proxyIdentifier, proxyIdentifierValue, perunAttrNames.getNames(), e);
            return res;
        }
        log.info("Processing facilities");
        Set<String> foundClientIds = new HashSet<>();
        for (FacilityWithAttributes f : facilities) {
            processFacility(f, foundClientIds, res);
        }
        if (proceedToDelete) {
            log.info("Removing old clients");
            deleteClients(foundClientIds, res);
        } else {
            log.warn("Script has disabled removing of old clients. " +
                "This might be due to Peruns unreachability! Check previous logs for more info.");
        }
        return res;
    }

    private void processFacility(FacilityWithAttributes f, Set<String> foundClientIds, SyncResult res) {
        try {
            if (f == null) {
                log.warn("NULL facility given, generating error and continue on processing");
                res.incErrors();
                return;
            }
            log.debug("Processing facility '{}'", f);
            boolean isOidc = f.getAttributes().get(perunAttrNames.getIsOidc()).valueAsBoolean();
            if (!isOidc) {
                log.debug("Service not marked as OIDC (attr isOidc), skip it.");
                return;
            }
            String clientId = f.getAttributes().get(perunAttrNames.getClientId()).valueAsString();
            if (!StringUtils.hasText(clientId)) {
                log.debug("ClientID is null, facility is probably not OIDC, skip it.");
                return;
            } else if (actionsProperties.getProtectedClientIds().contains(clientId)) {
                log.debug("ClientID is marked as protected in configuration, skip it.");
                return;
            }
            foundClientIds.add(clientId);
            MitreidClient mitreClient = getMitreClient(clientId);
            if (mitreClient == null) {
                log.info("No client found for client_id '{}' - create new", clientId);
                createClient(f.getAttributes(), res);
            } else {
                log.info("Existing client found for client_id '{}' - update it", clientId);
                updateClient(mitreClient, f.getAttributes(), res);
            }
            log.info("Client with id '{}' processed", clientId);
        } catch (Exception e) {
            log.warn("Caught exception when syncing facility {}", f, e);
            res.incErrors();
        }
    }

    private MitreidClient getMitreClient(String clientId) {
        MitreidClient mitreClient = clientRepository.getClientByClientId(clientId);
        log.debug("Got MitreID client");
        log.trace("{}", mitreClient);
        return mitreClient;
    }

    private Set<String> getClientIdsToDelete(Collection<String> foundClientIds) {
        Set<String> ids = clientRepository.getAllClientIdssWithNoParentClientId();
        ids.removeAll(foundClientIds);
        ids.removeAll(actionsProperties.getProtectedClientIds());
        return ids;
    }

    private void createClient(Map<String, PerunAttributeValue> attrs, SyncResult res)
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, InvalidPropertyException
    {
        if (actionsProperties.getToOidc().isCreate()) {
            MitreidClient c = new MitreidClient();
            setClientFields(c, attrs);
            c.setCreatedAt(new Date());
            if (interactiveMode) {
                System.out.println("Following client will be created");
                System.out.println(c);
                System.out.println(DO_YOU_WANT_TO_PROCEED);
                String response = scanner.nextLine();
                if (!Y.equalsIgnoreCase(response) && !YES.equalsIgnoreCase(response)) {
                    return;
                }
            }
            clientRepository.saveClient(c);
            log.debug("Client created");
            res.incCreated();
        } else {
            log.warn("Creating clients is disabled, skip creation");
        }
    }

    private void updateClient(MitreidClient original,
                              Map<String, PerunAttributeValue> attrs,
                              SyncResult res)
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException, InvalidPropertyException
    {
        if (actionsProperties.getToOidc().isUpdate()) {
            MitreidClient toUpdate;
            if (interactiveMode) {
                MitreidClient updated = clientRepository.getClientByClientId(original.getClientId());
                this.setClientFields(updated, attrs);
                DiffNode diff = ObjectDifferBuilder.buildDefault().compare(original, updated);
                if (diff.hasChanges()) {
                    System.out.println(SPACER);
                    diff.visit((node, visit) -> diffVisit(node, original, updated));
                    System.out.println(DO_YOU_WANT_TO_PROCEED);
                    String response = scanner.nextLine();
                    if (!Y.equalsIgnoreCase(response) && !YES.equalsIgnoreCase(response)) {
                        return;
                    } else {
                        System.out.println(SPACER);
                    }
                }
                toUpdate = updated;
                clientRepository.updateClient(original.getId(), updated);
            } else {
                this.setClientFields(original, attrs);
                toUpdate = original;
            }
            clientRepository.updateClient(original.getId(), toUpdate);
            log.debug("Client updated");
            res.incUpdated();
        } else {
            log.warn("Updating clients is disabled, skip update");
        }
    }

    private void diffVisit(DiffNode node, MitreidClient original, MitreidClient updated) {
        if (node.isRootNode()) {
            return;
        }
        Object baseValue = node.canonicalGet(original);
        Object workingValue = node.canonicalGet(updated);
        if (node.getParentNode().isRootNode()) {
            System.out.printf("Changes in field '%s'%n",
                    node.getElementSelector().toHumanReadableString());
            System.out.printf("  original: '%s'%n", baseValue);
            System.out.printf("  updated: '%s'%n", workingValue);
            System.out.println("  diff:");
        } else {
            if (baseValue == null) {
                System.out.printf("    added: '%s'%n", workingValue);
            } else if (workingValue == null) {
                System.out.printf("    removed: '%s'%n", baseValue);
            } else {
                System.out.printf("    changed: '%s' to: '%s'%n", baseValue, workingValue);
            }
        }
    }

    private void deleteClients(Set<String> foundClientIds, SyncResult res) {
        Set<String> clientsToDelete = getClientIdsToDelete(foundClientIds);
        if (actionsProperties.getToOidc().isDelete()) {
            if (interactiveMode) {
                for (String clientId: clientsToDelete) {
                    MitreidClient c = clientRepository.getClientByClientId(clientId);
                    System.out.println("About to remove following client");
                    System.out.println(c);
                    System.out.println(DO_YOU_WANT_TO_PROCEED);
                    String response = scanner.nextLine();
                    if (!Y.equalsIgnoreCase(response) && !YES.equalsIgnoreCase(response)) {
                        continue;
                    }
                    clientRepository.deleteClient(c);
                }
            } else {
                try {
                    log.debug("Deleting clients with ids {}", clientsToDelete);
                    int deleted = 0;
                    if (!clientsToDelete.isEmpty()) {
                        deleted += clientRepository.deleteByClientIds(clientsToDelete);
                    }
                    log.debug("Deleted {} clients", deleted);
                    res.incDeleted(deleted);
                } catch (Exception e) {
                    log.warn("Caught exception when deleting unused clients", e);
                    res.incErrors();
                }
            }
        } else {
            log.warn("Deleting of clients is disabled. Following clientIDs would be deleted: {}",
                clientsToDelete);
        }
    }

    private void setClientFields(MitreidClient c, Map<String, PerunAttributeValue> attrs)
        throws BadPaddingException, InvalidKeyException,
        IllegalBlockSizeException, InvalidPropertyException
    {
        setClientId(c, attrs);
        setClientSecret(c, attrs);
        setClientName(c, attrs);
        setClientDescription(c, attrs);
        setRedirectUris(c, attrs);
        setIntrospection(c, attrs);
        setPostLogoutRedirectUris(c, attrs);
        setScopes(c, attrs);
        setGrantAndResponseTypes(c, attrs);
        setPKCEOptions(c, attrs);
        setTokenEndpointAuthentication(c, attrs);
        setRefreshTokens(c, attrs);
        setTokenTimeouts(c, attrs);
        setPolicyUri(c, attrs);
        setContacts(c, attrs);
        setClientUri(c, attrs);
        setJurisdiction(c, attrs);
        setAcceptedTos(c, attrs);
        setResourceIds(c, attrs);
        setOnlyAllowedIdps(c, attrs);
        setBlockedIdps(c, attrs);
    }

    private void setClientId(MitreidClient c, Map<String, PerunAttributeValue> attrs)
        throws InvalidPropertyException
    {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getClientId(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        String clientId = attributeValue.valueAsString();
        if (!StringUtils.hasText(clientId)) {
            throw new InvalidPropertyException("Invalid client name: " + clientId);
        }
        c.setClientId(clientId);
    }

    private void setClientSecret(MitreidClient c, Map<String, PerunAttributeValue> attrs)
        throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException
    {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getClientSecret(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        String encryptedClientSecret = attributeValue.valueAsString();
        String clientSecret = "";
        if (StringUtils.hasText(encryptedClientSecret)) {
            clientSecret = Utils.decrypt(encryptedClientSecret, cipher, secretKeySpec);
        }
        c.setClientSecret(clientSecret);
    }

    private void setClientName(MitreidClient c, Map<String, PerunAttributeValue> attrs)
        throws InvalidPropertyException
    {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getName(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        String clientName = attributeValue.valueAsMap().getOrDefault("en", null);
        if (!StringUtils.hasText(clientName)) {
            throw new InvalidPropertyException("Invalid client name: " + clientName);
        }
        c.setClientName(clientName);
    }

    private void setClientDescription(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getDescription(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        String clientDescription = attributeValue.valueAsMap().getOrDefault("en", null);
        c.setClientDescription(clientDescription);
    }

    private void setRedirectUris(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getRedirectUris(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        Set<String> redirectUris = new HashSet<>(attributeValue.valueAsList());
        c.setRedirectUris(redirectUris);
    }

    private void setIntrospection(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        c.setAllowIntrospection(false);
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getIntrospection(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        boolean introspectionAllowed = attributeValue.valueAsBoolean();
        c.setAllowIntrospection(introspectionAllowed);
    }

    private void setPostLogoutRedirectUris(MitreidClient c,
                                           Map<String, PerunAttributeValue> attrs)
    {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getPostLogoutRedirectUris(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        Set<String> postLogoutRedirectUris = new HashSet<>(attributeValue.valueAsList());
        c.setPostLogoutRedirectUris(postLogoutRedirectUris);
    }

    private void setScopes(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getScopes(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        Set<String> scopes = new HashSet<>(attributeValue.valueAsList());
        c.setScope(scopes);
    }

    private void setGrantAndResponseTypes(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        List<String> grantTypesAttrValue =
            attrs.get(perunAttrNames.getGrantTypes()).valueAsList().stream()
                .map(String::toLowerCase).collect(Collectors.toList());

        Set<String> grantTypes = new HashSet<>();
        Set<String> responseTypes = new HashSet<>();

        if (grantTypesAttrValue.contains(AUTHORIZATION_CODE)) {
            grantTypes.add(GRANT_AUTHORIZATION_CODE);
            responseTypes.addAll(Arrays.asList(RESPONSE_TYPE_AUTH_CODE));
            log.debug("Added grant '{}' with response types '{}'", GRANT_AUTHORIZATION_CODE,
                RESPONSE_TYPE_AUTH_CODE);
        }

        if (grantTypesAttrValue.contains(IMPLICIT)) {
            grantTypes.add(GRANT_IMPLICIT);
            responseTypes.addAll(Arrays.asList(RESPONSE_TYPE_IMPLICIT));
            log.debug("Added grant '{}' with response types '{}'", GRANT_IMPLICIT,
                RESPONSE_TYPE_IMPLICIT);
        }

        if (grantTypesAttrValue.contains(HYBRID)) {
            grantTypes.add(GRANT_HYBRID);
            grantTypes.add(GRANT_AUTHORIZATION_CODE);
            responseTypes.addAll(Arrays.asList(RESPONSE_TYPE_HYBRID));
            log.debug("Added grants '{} {}' with response types '{}'", GRANT_HYBRID,
                GRANT_AUTHORIZATION_CODE,
                RESPONSE_TYPE_HYBRID);
        }

        if (grantTypesAttrValue.contains(CLIENT_CREDENTIALS)) {
            grantTypes.add(GRANT_CLIENT_CREDENTIALS);
            log.debug("Added grant '{}'", GRANT_CLIENT_CREDENTIALS);
        }

        if (grantTypesAttrValue.contains(DEVICE)) {
            grantTypes.add(GRANT_DEVICE);
            log.debug("Added grant '{}'", GRANT_DEVICE);
        }

        if (grantTypesAttrValue.contains(TOKEN_EXCHANGE)) {
            grantTypes.add(GRANT_TOKEN_EXCHANGE);
            log.debug("Added grant '{}", GRANT_TOKEN_EXCHANGE);
        }

        c.setGrantTypes(grantTypes);
        c.setResponseTypes(responseTypes);
    }

    private void setPKCEOptions(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        log.trace("Setting PKCE options");
        c.setCodeChallengeMethod(null);

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getCodeChallengeType(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }

        String codeChallengeType = attributeValue.valueAsString();
        if (!PKCE_TYPE_NONE.equalsIgnoreCase(codeChallengeType)) {
            log.debug("Code challenge requested is not equal to '{}'", PKCE_TYPE_NONE);
            if (PKCE_TYPE_PLAIN.equalsIgnoreCase(codeChallengeType)) {
                log.debug("Preparing for PKCE with challenge '{}'", PKCE_TYPE_PLAIN);
                c.setCodeChallengeMethod(PKCEAlgorithm.plain);
            } else if (PKCE_TYPE_SHA256.equalsIgnoreCase(codeChallengeType)) {
                log.debug("Preparing for PKCE with challenge '{}'", PKCE_TYPE_SHA256);
                c.setCodeChallengeMethod(PKCEAlgorithm.S256);
            }
        }
    }

    private void setTokenEndpointAuthentication(MitreidClient c,
                                                Map<String, PerunAttributeValue> attrs)
    {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getTokenEndpointAuthenticationMethod(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        String authMethodAttrValue = attributeValue.valueAsString();
        MitreidClient.AuthMethod authMethod = MitreidClient.AuthMethod.getByValue(authMethodAttrValue);
        if (authMethod == null) {
            log.debug("Failed to parse token endpoint authentication method." +
                " Using client_secret_basic as default value.");
            authMethod = MitreidClient.AuthMethod.SECRET_BASIC;
        }
        c.setTokenEndpointAuthMethod(authMethod);
        if (MitreidClient.AuthMethod.NONE.equals(authMethod)) {
            log.debug("NONE used as token endpoint authentication method. Removing client_secret");
            c.setClientSecret(null);
        }
    }

    private void setRefreshTokens(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        Set<String> grantTypes = c.getGrantTypes();
        if (grantTypes == null) {
            grantTypes = new HashSet<>();
        }
        if (grantAllowsRefreshTokens(grantTypes)) {
            PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getIssueRefreshTokens(), null);
            boolean requestedViaAttr;
            if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
                requestedViaAttr = false;
            } else {
                requestedViaAttr = attributeValue.valueAsBoolean();
            }
            boolean requestedViaScopes = c.getScope().contains(OFFLINE_ACCESS);
            log.debug("Refresh tokens requested via: attr({}), scopes({})", requestedViaAttr, requestedViaScopes);
            if (requestedViaAttr || requestedViaScopes) {
                setUpRefreshTokens(c, attrs);
            }
        }
    }

    private void setUpRefreshTokens(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        c.getScope().add(OFFLINE_ACCESS);
        c.getGrantTypes().add(GRANT_REFRESH_TOKEN);
        c.setClearAccessTokensOnRefresh(true);
        c.setReuseRefreshToken(false);
        if (StringUtils.hasText(perunAttrNames.getReuseRefreshTokens())) {
            PerunAttributeValue attributeValue =
                attrs.getOrDefault(perunAttrNames.getReuseRefreshTokens(), null);
            if (attributeValue != null &&
                !PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
                c.setReuseRefreshToken(attributeValue.valueAsBoolean());
            }
        }
    }

    private boolean grantAllowsRefreshTokens(Set<String> grantTypes) {
        boolean res = !grantTypes.isEmpty()
                && (grantTypes.contains(GRANT_DEVICE)
                || grantTypes.contains(GRANT_AUTHORIZATION_CODE)
                || grantTypes.contains(GRANT_HYBRID));
        log.debug("Grants '{}' {} issuing refresh tokens", grantTypes, res ? "allow" : "disallow");
        return res;
    }

    private void setTokenTimeouts(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        Set<String> grantTypes = c.getGrantTypes();
        Map<String, String> attrValueAsMap = new HashMap<>();
        if (StringUtils.hasText(perunAttrNames.getTokenTimeouts())) {
            PerunAttributeValue attrValue =
                attrs.getOrDefault(perunAttrNames.getTokenTimeouts(), null);
            if (attrValue != null && !PerunAttributeValueAwareModel.isNullValue(attrValue.getValue())) {
                attrValueAsMap = attrValue.valueAsMap();
            }
        }
        Map<String, CredentialTimeout> tokenTimeouts = new HashMap<>();
        fillDefaultTokenTimeouts(tokenTimeouts, grantTypes);
        fillRequestTokenTimeouts(tokenTimeouts, attrValueAsMap);

        c.setAccessTokenValiditySeconds(
            tokenTimeouts.getOrDefault(ACCESS_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE)
                .computeTimeout()
        );
        c.setIdTokenValiditySeconds(
            tokenTimeouts.getOrDefault(ID_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE)
                .computeTimeout()
        );
        c.setRefreshTokenValiditySeconds(
            tokenTimeouts.getOrDefault(REFRESH_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE)
                .computeTimeout()
        );
        c.setDeviceCodeValiditySeconds(
            tokenTimeouts.getOrDefault(DEVICE_CODE_TIMEOUT, CredentialTimeout.NO_VALUE)
                .computeTimeout()
        );
    }

    private void fillRequestTokenTimeouts(Map<String, CredentialTimeout> tokenTimeouts,
                                          Map<String, String> attrValueAsMap)
    {
        if (attrValueAsMap.containsKey(ACCESS_TOKEN_TIMEOUT)) {
            tokenTimeouts.put(
                ACCESS_TOKEN_TIMEOUT,
                CredentialTimeout.fromStringValue(attrValueAsMap.get(ACCESS_TOKEN_TIMEOUT))
            );
        }
        if (attrValueAsMap.containsKey(ID_TOKEN_TIMEOUT)) {
            tokenTimeouts.put(
                ID_TOKEN_TIMEOUT,
                CredentialTimeout.fromStringValue(attrValueAsMap.get(ID_TOKEN_TIMEOUT))
            );
        }
        if (attrValueAsMap.containsKey(REFRESH_TOKEN_TIMEOUT)) {
            tokenTimeouts.put(
                REFRESH_TOKEN_TIMEOUT,
                CredentialTimeout.fromStringValue(attrValueAsMap.get(REFRESH_TOKEN_TIMEOUT))
            );
        }
        if (attrValueAsMap.containsKey(DEVICE_CODE_TIMEOUT)) {
            tokenTimeouts.put(
                DEVICE_CODE_TIMEOUT,
                CredentialTimeout.fromStringValue(attrValueAsMap.get(DEVICE_CODE_TIMEOUT))
            );
        }
    }

    private void fillDefaultTokenTimeouts(Map<String, CredentialTimeout> tokenTimeouts,
                                          Set<String> grantTypes)
    {
        if (grantTypes.contains(GRANT_AUTHORIZATION_CODE)) {
            fillTokenTimeoutForGrant(
                credentialTimeoutByGrantProps.getAuthorizationCode(), tokenTimeouts);
        }
        if (grantTypes.contains(GRANT_IMPLICIT)) {
            fillTokenTimeoutForGrant(
                credentialTimeoutByGrantProps.getImplicit(), tokenTimeouts);
        }
        if (grantTypes.contains(GRANT_HYBRID)) {
            fillTokenTimeoutForGrant(
                credentialTimeoutByGrantProps.getHybrid(), tokenTimeouts);
        }
        if (grantTypes.contains(GRANT_DEVICE)) {
            fillTokenTimeoutForGrant(
                credentialTimeoutByGrantProps.getDevice(), tokenTimeouts);
        }
        if (grantTypes.contains(GRANT_CLIENT_CREDENTIALS)) {
            fillTokenTimeoutForGrant(
                credentialTimeoutByGrantProps.getClientCredentials(), tokenTimeouts);
        }
    }

    private void fillTokenTimeoutForGrant(
            GrantTypesTimeoutsProperties.GrantType grantType,
            Map<String, CredentialTimeout> tokenTimeouts
    ) {
        int currentTimeout;
        int grantTimeout;

        // access token
        currentTimeout = tokenTimeouts.getOrDefault(
                ACCESS_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE
            ).computeTimeout();
        grantTimeout = grantType.getAccessToken().computeTimeout();
        if (grantTimeout > currentTimeout) {
            tokenTimeouts.put(ACCESS_TOKEN_TIMEOUT, grantType.getAccessToken());
        }

        // id token
        currentTimeout = tokenTimeouts.getOrDefault(
                ID_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE
            ).computeTimeout();
        grantTimeout = grantType.getIdToken().computeTimeout();
        if (grantTimeout > currentTimeout) {
            tokenTimeouts.put(ID_TOKEN_TIMEOUT, grantType.getIdToken());
        }

        // refresh token
        currentTimeout = tokenTimeouts.getOrDefault(
                REFRESH_TOKEN_TIMEOUT, CredentialTimeout.NO_VALUE
            ).computeTimeout();
        grantTimeout = grantType.getRefreshToken().computeTimeout();
        if (grantTimeout > currentTimeout) {
            tokenTimeouts.put(REFRESH_TOKEN_TIMEOUT, grantType.getRefreshToken());
        }

        // device code
        currentTimeout = tokenTimeouts.getOrDefault(
                DEVICE_CODE_TIMEOUT, CredentialTimeout.NO_VALUE
            ).computeTimeout();
        grantTimeout = grantType.getDeviceCode().computeTimeout();
        if (grantTimeout > currentTimeout) {
            tokenTimeouts.put(DEVICE_CODE_TIMEOUT, grantType.getDeviceCode());
        }
    }

    private void setPolicyUri(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getPrivacyPolicy(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            return;
        }
        if (PerunAttributeValueAwareModel.MAP_TYPE.equals(attributeValue.getType())) {
            c.setPolicyUri(attributeValue.valueAsMap().getOrDefault("en", ""));
        } else if (PerunAttributeValueAwareModel.STRING_TYPE.equals(attributeValue.getType())) {
            c.setPolicyUri(attributeValue.valueAsString());
        } else {
            c.setPolicyUri("");
        }
    }

    private void setContacts(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        Set<String> contacts = new TreeSet<>();
        for (String attr: perunAttrNames.getContacts()) {
            if (StringUtils.hasText(attr)) {
                //in case of string attribute, it will be returned as single-item array
                PerunAttributeValue attributeValue = attrs.getOrDefault(attr, null);
                if (attributeValue != null && !PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
                    contacts.addAll(attributeValue.valueAsList());
                }
            }
        }
        c.setContacts(contacts);
    }

    private void setClientUri(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null
            || perunAttrNames.getHomePageUris() == null
            || perunAttrNames.getHomePageUris().isEmpty())
        {
            return;
        }

        for (String attr: perunAttrNames.getHomePageUris()) {
            if (StringUtils.hasText(attr)) {
                PerunAttributeValue attributeValue = attrs.getOrDefault(attr, null);
                if (attributeValue != null && !PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
                    String value = "";
                    if (PerunAttributeValueAwareModel.MAP_TYPE.equals(attributeValue.getType()) &&
                        StringUtils.hasText(attributeValue.valueAsMap().getOrDefault("en", ""))) {
                        value = attributeValue.valueAsMap().getOrDefault("en", "");
                    } else if (PerunAttributeValueAwareModel.STRING_TYPE.equals(attributeValue.getType())
                            && StringUtils.hasText(attributeValue.valueAsString())) {
                        value = attributeValue.valueAsString();
                    }

                    if (StringUtils.hasText(value)) {
                        c.setClientUri(value);
                        break;
                    }
                }
            }
        }
    }

    private void setJurisdiction(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null || !StringUtils.hasText(perunAttrNames.getJurisdiction())) {
            return;
        }

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getJurisdiction(), null);
        if (attributeValue != null && !PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            c.setJurisdiction(attributeValue.valueAsString());
        }
    }

    private void setAcceptedTos(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null || !StringUtils.hasText(perunAttrNames.getAcceptedTos())) {
            return;
        }

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getAcceptedTos(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            c.setAcceptedTos(false);
        } else {
            c.setAcceptedTos(attributeValue.valueAsBoolean());
        }
    }

    private void setResourceIds(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null || !StringUtils.hasText(perunAttrNames.getResourceIds())) {
            return;
        }

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getResourceIds(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            c.setResourceIds(new HashSet<>());
        } else {
            c.setResourceIds(new HashSet<>(attributeValue.valueAsList()));
        }
    }

    private void setOnlyAllowedIdps(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null || !StringUtils.hasText(perunAttrNames.getOnlyAllowedIdps())) {
            return;
        }

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getOnlyAllowedIdps(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            c.setOnlyAllowedIdps(new HashSet<>());
        } else {
            c.setOnlyAllowedIdps(new HashSet<>(attributeValue.valueAsList()));
        }
    }

    private void setBlockedIdps(MitreidClient c, Map<String, PerunAttributeValue> attrs) {
        if (attrs == null || !StringUtils.hasText(perunAttrNames.getBlockedIdps())) {
            return;
        }

        PerunAttributeValue attributeValue = attrs.getOrDefault(perunAttrNames.getBlockedIdps(), null);
        if (attributeValue == null || PerunAttributeValueAwareModel.isNullValue(attributeValue.getValue())) {
            c.setBlockedIdps(new HashSet<>());
        } else {
            c.setBlockedIdps(new HashSet<>(attributeValue.valueAsList()));
        }
    }

}
