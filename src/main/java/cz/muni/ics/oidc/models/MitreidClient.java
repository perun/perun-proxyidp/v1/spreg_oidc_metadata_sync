package cz.muni.ics.oidc.models;


import static cz.muni.ics.oidc.models.MitreidClient.PARAM_PARENT_CLIENT_ID;
import static cz.muni.ics.oidc.models.MitreidClient.QUERY_ALL_BY_PARENT_CLIENT_ID;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jwt.JWT;
import cz.muni.ics.oidc.models.converters.JWEAlgorithmStringConverter;
import cz.muni.ics.oidc.models.converters.JWEEncryptionMethodStringConverter;
import cz.muni.ics.oidc.models.converters.JWKSetStringConverter;
import cz.muni.ics.oidc.models.converters.JWSAlgorithmStringConverter;
import cz.muni.ics.oidc.models.converters.JWTStringConverter;
import cz.muni.ics.oidc.models.converters.PKCEAlgorithmStringConverter;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.hibernate.Hibernate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "client_details")
@NamedQueries({
        @NamedQuery(name = MitreidClient.QUERY_ALL,
                query = "SELECT c FROM MitreidClient c"),
        @NamedQuery(name = MitreidClient.QUERY_BY_CLIENT_ID,
                query = "SELECT c FROM MitreidClient c " +
                        "WHERE c.clientId = :" + MitreidClient.PARAM_CLIENT_ID),
        @NamedQuery(name = MitreidClient.QUERY_ALL_CLIENT_IDS_WITH_NO_PARENT_CLIENT,
                query = "SELECT c.clientId FROM MitreidClient c WHERE c.parentClientId IS NULL"),
        @NamedQuery(name = MitreidClient.DELETE_BY_CLIENT_IDS,
                query = "DELETE FROM MitreidClient c " +
                        "WHERE c.clientId IN :" + MitreidClient.PARAM_CLIENT_ID_SET
        ),
        @NamedQuery(name = QUERY_ALL_BY_PARENT_CLIENT_ID,
            query = "SELECT c FROM MitreidClient c " +
            "WHERE c.parentClientId = :" + PARAM_PARENT_CLIENT_ID),
})
public class MitreidClient implements ClientDetails {

    public static final String QUERY_BY_CLIENT_ID = "MitreidClient.getByClientId";
    public static final String QUERY_ALL = "MitreidClient.findAll";
    public static final String QUERY_ALL_CLIENT_IDS_WITH_NO_PARENT_CLIENT =
        "MitreidClient.getAllClientIdssWithNoParentClientId";
    public static final String DELETE_BY_CLIENT_IDS = "MitreidClient.deleteByClientIds";
    public static final String QUERY_ALL_BY_PARENT_CLIENT_ID = "MitreidClient.getByParentClientId";

    public static final String PARAM_CLIENT_ID = "clientId";
    public static final String PARAM_CLIENT_ID_SET = "clientIdSet";
    public static final String PARAM_PARENT_CLIENT_ID = "parentClientId";

    private static final int DEFAULT_ID_TOKEN_VALIDITY_SECONDS = 600;
    private static final long serialVersionUID = -1617727085733786296L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "client_description")
    private String clientDescription = "";

    @Column(name = "client_id")
    private String clientId = null;

    @Column(name = "client_secret")
    private String clientSecret = null;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_redirect_uri", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "redirect_uri")
    @CascadeOnDelete
    private Set<String> redirectUris = new HashSet<>();

    @Column(name = "client_uri")
    private String clientUri;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_contact", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "contact")
    @CascadeOnDelete
    private Set<String> contacts = new HashSet<>();

    @Column(name = "tos_uri")
    private String tosUri;

    @Enumerated(EnumType.STRING)
    @Column(name = "token_endpoint_auth_method")
    private AuthMethod tokenEndpointAuthMethod = AuthMethod.SECRET_BASIC;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_scope", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "scope")
    @CascadeOnDelete
    private Set<String> scope = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_grant_type", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "grant_type")
    @CascadeOnDelete
    private Set<String> grantTypes = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_response_type", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "response_type")
    @CascadeOnDelete
    private Set<String> responseTypes = new HashSet<>();

    @Column(name = "policy_uri")
    private String policyUri;

    @Column(name = "jwks_uri")
    private String jwksUri;

    @Column(name = "jwks")
    @Convert(converter = JWKSetStringConverter.class)
    private JWKSet jwks;

    @Column(name = "software_id")
    private String softwareId;

    @Column(name = "software_version")
    private String softwareVersion;

    @Enumerated(EnumType.STRING)
    @Column(name = "application_type")
    private AppType applicationType;

    @Column(name = "sector_identifier_uri")
    private String sectorIdentifierUri;

    @Enumerated(EnumType.STRING)
    @Column(name = "subject_type")
    private SubjectType subjectType;

    @Column(name = "request_object_signing_alg")
    @Convert(converter = JWSAlgorithmStringConverter.class)
    private JWSAlgorithm requestObjectSigningAlg = null;

    @Column(name = "user_info_signed_response_alg")
    @Convert(converter = JWSAlgorithmStringConverter.class)
    private JWSAlgorithm userInfoSignedResponseAlg = null;

    @Column(name = "user_info_encrypted_response_alg")
    @Convert(converter = JWEAlgorithmStringConverter.class)
    private JWEAlgorithm userInfoEncryptedResponseAlg = null;

    @Column(name = "user_info_encrypted_response_enc")
    @Convert(converter = JWEEncryptionMethodStringConverter.class)
    private EncryptionMethod userInfoEncryptedResponseEnc = null;

    @Column(name = "id_token_signed_response_alg")
    @Convert(converter = JWSAlgorithmStringConverter.class)
    private JWSAlgorithm idTokenSignedResponseAlg = null;

    @Column(name = "id_token_encrypted_response_alg")
    @Convert(converter = JWEAlgorithmStringConverter.class)
    private JWEAlgorithm idTokenEncryptedResponseAlg = null;

    @Column(name = "id_token_encrypted_response_enc")
    @Convert(converter = JWEEncryptionMethodStringConverter.class)
    private EncryptionMethod idTokenEncryptedResponseEnc = null;

    @Column(name = "token_endpoint_auth_signing_alg")
    @Convert(converter = JWSAlgorithmStringConverter.class)
    private JWSAlgorithm tokenEndpointAuthSigningAlg = null;

    @Column(name = "default_max_age")
    private Integer defaultMaxAge;

    @Column(name = "require_auth_time")
    private Boolean requireAuthTime;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_default_acr_value", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "default_acr_value")
    @CascadeOnDelete
    private Set<String> defaultACRvalues;

    @Column(name = "initiate_login_uri")
    private String initiateLoginUri;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_post_logout_redirect_uri", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "post_logout_redirect_uri")
    @CascadeOnDelete
    private Set<String> postLogoutRedirectUris = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_request_uri", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "request_uri")
    @CascadeOnDelete
    private Set<String> requestUris = new HashSet<>();;

    @Column(name = "access_token_validity_seconds")
    private Integer accessTokenValiditySeconds = 0;

    @Column(name = "refresh_token_validity_seconds")
    private Integer refreshTokenValiditySeconds = 0;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_resource", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "resource_id")
    @CascadeOnDelete
    private Set<String> resourceIds = new HashSet<>();

    @Column(name = "reuse_refresh_tokens")
    private boolean reuseRefreshToken = true;

    @Column(name = "dynamically_registered")
    private boolean dynamicallyRegistered = false;

    @Column(name = "allow_introspection")
    private boolean allowIntrospection = false;

    @Column(name = "id_token_validity_seconds")
    private Integer idTokenValiditySeconds = DEFAULT_ID_TOKEN_VALIDITY_SECONDS;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_at")
    private Date createdAt = new Date();

    @Column(name = "clear_access_tokens_on_refresh")
    private boolean clearAccessTokensOnRefresh = true;

    @Column(name = "device_code_validity_seconds")
    private Integer deviceCodeValiditySeconds = 0;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_claims_redirect_uri", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "redirect_uri")
    @CascadeOnDelete
    private Set<String> claimsRedirectUris = new HashSet<>();

    @Column(name = "software_statement")
    @Convert(converter = JWTStringConverter.class)
    private JWT softwareStatement;

    @Column(name = "code_challenge_method")
    @Convert(converter = PKCEAlgorithmStringConverter.class)
    private PKCEAlgorithm codeChallengeMethod;

    @Column(name = "accepted_tos")
    private boolean acceptedTos;

    @Column(name = "jurisdiction")
    private String jurisdiction;

    @Column(name = "parent_client_id")
    private Long parentClientId;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_only_allowed_idps", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "idp_entity_id")
    @CascadeOnDelete
    private Set<String> onlyAllowedIdps;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "client_blocked_idps", joinColumns = @JoinColumn(name = "owner_id"))
    @Column(name = "idp_entity_id")
    @CascadeOnDelete
    private Set<String> blockedIdps;

    @Transient
    private Map<String, Object> additionalInformation = new HashMap<>();

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if (getIdTokenValiditySeconds() == null) {
            setIdTokenValiditySeconds(DEFAULT_ID_TOKEN_VALIDITY_SECONDS);
        }
    }

    @Override
    public boolean isSecretRequired() {
        return false;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return null;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return null;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return new HashSet<>();
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return additionalInformation;
    }

    public enum AuthMethod {
        SECRET_POST("client_secret_post"),
        SECRET_BASIC("client_secret_basic"),
        SECRET_JWT("client_secret_jwt"),
        PRIVATE_KEY("private_key_jwt"),
        NONE("none");

        private final String value;

        // map to aid reverse lookup
        private static final Map<String, AuthMethod> lookup = new HashMap<>();
        static {
            for (AuthMethod a : AuthMethod.values()) {
                lookup.put(a.getValue(), a);
            }
        }

        AuthMethod(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static AuthMethod getByValue(String value) {
            return lookup.get(value);
        }
    }

    public enum AppType {
        WEB("web"), NATIVE("native");

        private final String value;

        // map to aid reverse lookup
        private static final Map<String, AppType> lookup = new HashMap<>();
        static {
            for (AppType a : AppType.values()) {
                lookup.put(a.getValue(), a);
            }
        }

        AppType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static AppType getByValue(String value) {
            return lookup.get(value);
        }
    }

    public enum SubjectType {
        PAIRWISE("pairwise"), PUBLIC("public");

        private final String value;

        // map to aid reverse lookup
        private static final Map<String, SubjectType> lookup = new HashMap<>();
        static {
            for (SubjectType u : SubjectType.values()) {
                lookup.put(u.getValue(), u);
            }
        }

        SubjectType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static SubjectType getByValue(String value) {
            return lookup.get(value);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
            Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        MitreidClient that = (MitreidClient) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
