package cz.muni.ics.oidc.models;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

/**
 * Facility with attributes object model.
 *
 * @author Dominik Baranek <baranek@.ics.muni.cz>
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class FacilityWithAttributes {

    @NonNull
    private Facility facility;

    private Map<String, PerunAttributeValue> attributes;

}
