package cz.muni.ics.oidc.props;

import cz.muni.ics.oidc.enums.CredentialTimeoutTimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.util.StringUtils;

@Getter
@ToString
@EqualsAndHashCode
public class CredentialTimeout {

    public static final CredentialTimeout NO_VALUE = new CredentialTimeout(0, CredentialTimeoutTimeUnit.SECONDS);

    private static final Pattern REGEX = Pattern.compile("(\\d+)([shmdwy])?");

    private final int timeout;

    private final CredentialTimeoutTimeUnit unit;

    @ConstructorBinding
    public CredentialTimeout(int timeout, CredentialTimeoutTimeUnit unit) {
        if (timeout < 0) {
            throw new IllegalArgumentException("timeout must be positive number");
        } else if (unit == null) {
            throw new IllegalArgumentException("unit must not be null");
        }
        this.unit = unit;
        this.timeout = timeout;
    }

    public CredentialTimeout(int timeout) {
        this(timeout, CredentialTimeoutTimeUnit.SECONDS);
    }

    public int computeTimeout() {
        return timeout * getMultiplier(unit);
    }

    public static CredentialTimeout fromStringValue(String value) {
        if (!StringUtils.hasText(value)) {
            return NO_VALUE;
        }
        Matcher matcher = REGEX.matcher(value);
        if (!matcher.matches()) {
            return NO_VALUE;
        }
        String timeoutStr = matcher.group(1);
        int timeout = 0;
        if (StringUtils.hasText(timeoutStr)) {
            timeout = Integer.parseInt(timeoutStr);
        }
        if (timeout <= 0) {
            return NO_VALUE;
        }

        String unitStr = matcher.group(2);
        CredentialTimeoutTimeUnit unit = CredentialTimeoutTimeUnit.SECONDS;
        if (StringUtils.hasText(unitStr)) {
            unit = CredentialTimeoutTimeUnit.resolve(unitStr.charAt(0));
        }

        return new CredentialTimeout(timeout, unit);
    }

    private int getMultiplier(CredentialTimeoutTimeUnit unit) {
        switch (unit) {
            case MINUTES:
                return 60;
            case HOURS:
                return 60 * 60;
            case DAYS:
                return 24 * 60 * 60;
            case WEEKS:
                return 7 * 24 * 60 * 60;
            case YEARS:
                return 365 * 7 * 24 * 60 * 60;
            case SECONDS:
            default:
                return 1;
        }
    }

}
