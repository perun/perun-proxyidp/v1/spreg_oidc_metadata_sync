package cz.muni.ics.oidc.props;

import com.fasterxml.jackson.annotation.JsonAlias;
import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "tokens", ignoreInvalidFields = true)
@Configuration
@Slf4j
public class GrantTypesTimeoutsProperties {

    @JsonAlias("authorization_code")
    @NotNull private GrantType authorizationCode = new GrantType(
        new CredentialTimeout(3600),
        new CredentialTimeout(3600),
        new CredentialTimeout(7200),
        new CredentialTimeout(0)
    );
    @JsonAlias("implicit")
    @NotNull private GrantType implicit = new GrantType(
        new CredentialTimeout(14400),
        new CredentialTimeout(14400),
        new CredentialTimeout(0),
        new CredentialTimeout(0)
    );
    @JsonAlias("hybrid")
    @NotNull private GrantType hybrid = new GrantType(
        new CredentialTimeout(14400),
        new CredentialTimeout(14400),
        new CredentialTimeout(28800),
        new CredentialTimeout(0)
    );
    @JsonAlias("device")
    @NotNull private GrantType device = new GrantType(
        new CredentialTimeout(3600),
        new CredentialTimeout(3600),
        new CredentialTimeout(7200),
        new CredentialTimeout(600)
    );
    @JsonAlias("client_credentials")
    @NotNull private GrantType clientCredentials = new GrantType(
        new CredentialTimeout(3600),
        new CredentialTimeout(0),
        new CredentialTimeout(7200),
        new CredentialTimeout(6000)
    );

    @Getter
    @ToString
    @EqualsAndHashCode
    public static class GrantType {
        private final CredentialTimeout accessToken;
        private final CredentialTimeout idToken;
        private final CredentialTimeout refreshToken;
        private final CredentialTimeout deviceCode;

        @ConstructorBinding
        public GrantType(CredentialTimeout accessToken,
                         CredentialTimeout idToken,
                         CredentialTimeout refreshToken,
                         CredentialTimeout deviceCode)
        {
            if (accessToken == null) {
                accessToken = new CredentialTimeout(0);
            }
            if (idToken == null) {
                idToken = new CredentialTimeout(0);
            }
            if (refreshToken == null) {
                refreshToken = new CredentialTimeout(0);
            }
            if (deviceCode == null) {
                deviceCode = new CredentialTimeout(0);
            }
            this.accessToken = accessToken;
            this.idToken = idToken;
            this.refreshToken = refreshToken;
            this.deviceCode = deviceCode;
        }

    }

    @PostConstruct
    public void init() {
        log.info("Initialized {}", this.getClass().getName());
        log.debug("{}", this);
    }

}
