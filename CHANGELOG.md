## [6.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v6.1.0...v6.1.1) (2024-04-23)


### Bug Fixes

* 🐛 Correctly set post_logout_redirect_uris (used attr fix) ([875c5e5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/875c5e5359eb262076de953337699e88c46ec66d))

# [6.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v6.0.0...v6.1.0) (2024-03-19)


### Features

* 🎸 Sync blocked and allowed IdPs ([594e43b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/594e43b44813888950a888ef2716a30422212972))

# [6.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.6...v6.0.0) (2024-03-01)


### Features

* 🎸 support time units in token timeouts ([4fa8c8f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/4fa8c8f6651cd5443e323482834d2dfe0db9ab0c))


### BREAKING CHANGES

* 🧨 Configuration strucutre for tokenTimeouts has changed (see template for
format)

## [5.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.5...v5.0.6) (2024-02-29)


### Bug Fixes

* 🐛 Fix token timeout attr name pressence check ([4a52016](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/4a52016ab7c4332627a54f976d9d47a02fe66927))

## [5.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.4...v5.0.5) (2024-02-19)


### Bug Fixes

* 🐛 Used wrong attribute name for several fields ([6b9aad3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/6b9aad35d1288c490591b1e7228b0f5f4acfae71))

## [5.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.3...v5.0.4) (2024-02-13)


### Bug Fixes

* 🐛 Fix missing resourceId setting in sync ([035a094](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/035a0948e1555ae636a0f563bc580d99311faf24))

## [5.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.2...v5.0.3) (2024-01-12)


### Bug Fixes

* 🐛 Fix query - lookup of clients without parentId ([b0f26a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/b0f26a54200b0efe2902731240991073644f2e12))

## [5.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.1...v5.0.2) (2024-01-12)


### Bug Fixes

* 🐛 Fix client_crednetials grant tokens length ([2dae30e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/2dae30eb407b8b25acdf32a14aa034f334ed326d))

## [5.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v5.0.0...v5.0.1) (2024-01-12)


### Bug Fixes

* 🐛 Use isOidc in ToOIDCSync ([9a5a78d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/9a5a78dc1e62aebe2c267ab557be87e5b5fc5a6c))

# [5.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.4.0...v5.0.0) (2023-12-27)


### Features

* 🎸 Remove child clients when removing parent ([bbb3694](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/bbb36948ed5cb1f41e6de2cbd3bf8baeda32ed17))


### BREAKING CHANGES

* 🧨 Requires MitreID v17.0.0

# [4.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.3.1...v4.4.0) (2023-11-13)


### Features

* 🎸 support for client_credentials grant type ([1875c3e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/1875c3ed439714c8a28f32b80ce593d7ff461550))

## [4.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.3.0...v4.3.1) (2023-11-13)


### Bug Fixes

* 🐛 Fix fetching resourceIds param ([44571ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/44571ad635cd059cae092655f38acf7bb1527e94))

# [4.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.8...v4.3.0) (2023-07-26)


### Features

* 🎸 Sync resource IDs ([9610de7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/9610de7a2eb31e0cd98389c0383de2e77825197d))

## [4.2.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.7...v4.2.8) (2023-06-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.13 ([f0ebd0f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/f0ebd0fa00490ee86a1a4cc8bcbea112dcb35ecc))

## [4.2.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.6...v4.2.7) (2023-05-21)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.12 ([9b6de3a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/9b6de3a4ab316284bd771c8b0d66b5527bcca376))

## [4.2.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.5...v4.2.6) (2023-04-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.11 ([0ed5ed0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/0ed5ed09610ab4737c51b5e06f1163a302a557f3))

## [4.2.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.4...v4.2.5) (2023-03-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.10 ([febf7a9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/febf7a922f43ceb5f88bf0cdf83bf56bf06ae9f1))

## [4.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.3...v4.2.4) (2023-02-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.9 ([140905c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/140905cc75458767c6ab439823d05fb0aba3b5b8))

## [4.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.2...v4.2.3) (2023-02-18)


### Bug Fixes

* **deps:** update dependency org.eclipse.persistence:org.eclipse.persistence.jpa to v2.7.12 ([e2dae3f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/e2dae3f7d8631f98ca9fe42bdbaba059fdd4f1eb))

## [4.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.1...v4.2.2) (2023-01-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.8 ([92c4e1c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/92c4e1cd25b98afb33a194f8ec67c3cacca449fb))

## [4.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.2.0...v4.2.1) (2023-01-25)


### Bug Fixes

* 🐛 Update MySQL dependency after its transfer ([a19f563](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/a19f5633e8fbe5add0ffd3283441ee97b8bd16ba))

# [4.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.1.2...v4.2.0) (2023-01-19)


### Features

* 🎸 Introducing TOKEN EXCHANGE grant ([178b86d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/178b86de4aba94255b25df49264f3649c0056950))

## [4.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.1.1...v4.1.2) (2022-12-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.7 ([843906d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/843906d6c547d75be27c048207774bbd5736b25e))

## [4.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.1.0...v4.1.1) (2022-12-16)


### Bug Fixes

* 🐛 Update model for MitreIdClient ([28ea078](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/28ea0785f2836942404ece63495be9a1528d3227))

# [4.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.7...v4.1.0) (2022-12-13)


### Features

* 🎸 Sync jurisdiction and acceptedTos ([ee999b7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/ee999b72e6060edcb31c09e29661d45d4f12b478))

## [4.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.6...v4.0.7) (2022-11-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.6 ([c769ab2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/c769ab26efcdd15836b470172f9bed3fc1341c14))

## [4.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.5...v4.0.6) (2022-10-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.5 ([04abff0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/04abff02421e114ed22dbc8304caa4a163e929a3))

## [4.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.4...v4.0.5) (2022-10-16)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.6 ([6869f4c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/6869f4cad6273fdbc126a55b19fc9248c3331b5e))

## [4.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.3...v4.0.4) (2022-10-06)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([606d457](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/606d457c5fd1edbf00d11b4247cc843fa30057a4))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([f28794e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/f28794e630b1753dee3177828b6fe2de6feaa051))

## [4.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.2...v4.0.3) (2022-10-04)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([12aceb2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/12aceb261b8bcef8cdfd75c42502488acb0c8d34))

## [4.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.1...v4.0.2) (2022-10-04)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([f711fa6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/f711fa649ebceac5c34c16c9bb75c9582beda332))

## [4.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/compare/v4.0.0...v4.0.1) (2022-09-28)


### Bug Fixes

* first release in GitLab ([8836af3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/spreg_oidc_metadata_sync/commit/8836af3234c8cfa86e8cf73dfa6f733bdb45c484))

# [4.0.0](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.16...v4.0.0) (2022-09-19)


### Features

* 🎸 Sync Token endpoint authN method ([a06ae5a](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/a06ae5aa92fe95e59d5d77ae8b0542fa61217e51))


### BREAKING CHANGES

* 🧨 Needs configuration for new attributes

## [3.1.16](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.15...v3.1.16) (2022-09-13)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25 ([5334906](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/53349069e5a1059e0df5ba61aa87fc669d698bca))

## [3.1.15](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.14...v3.1.15) (2022-08-29)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.24.3 ([b2e0483](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/b2e0483c66a0bba272201038ef7937b38def584b))

## [3.1.14](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.13...v3.1.14) (2022-08-25)


### Bug Fixes

* 🐛 Allow PKCE for devicecode ([2de57f2](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/2de57f26844a9aa44fc80c598db32f8439dbc3cb))

## [3.1.13](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.12...v3.1.13) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.3 ([a058934](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/a058934bd17b2ec1900e31781cac75f35180c285))

## [3.1.12](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.11...v3.1.12) (2022-08-20)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.24.2 ([665f4ae](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/665f4ae84f8ed38170bda2bed4abf7fbdc71c48d))

## [3.1.11](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.10...v3.1.11) (2022-08-15)


### Bug Fixes

* **deps:** update dependency org.eclipse.persistence:org.eclipse.persistence.jpa to v2.7.11 ([9b1db36](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/9b1db36ec0e31b09a6cafda5df080f5a76c2f8db))

## [3.1.10](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.9...v3.1.10) (2022-07-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.2 ([4c92855](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/4c928556a263c7ac671d912d1df52e220e8c2680))

## [3.1.9](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.8...v3.1.9) (2022-06-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.1 ([e73df5f](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/e73df5f6f9fd196783843951e9a7e114d7b56c40))

## [3.1.8](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.7...v3.1.8) (2022-06-01)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.23 ([417063c](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/417063ced01c1ee1c1535677b35f0b94f7c9cbb3))

## [3.1.7](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.6...v3.1.7) (2022-05-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.0 ([b7ea368](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/b7ea3688ec7d18a68f741fae8d5d735905cfa862))

## [3.1.6](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.5...v3.1.6) (2022-04-25)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.22 ([6586bb2](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/6586bb27231b2009c1aaf113d708609b45cef18f))

## [3.1.5](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.4...v3.1.5) (2022-04-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.7 ([0bfc68c](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/0bfc68c86d492528e66e9735fb7576b0fca80c17))
* **deps:** update dependency org.springframework.security.oauth:spring-security-oauth2 to v2.5.2.release ([548d796](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/548d796250734e9b3209b9a6f0fd2e6eb64290ac))

## [3.1.4](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.3...v3.1.4) (2022-03-31)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.6 ([c3a5e68](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/c3a5e684e7bc34e70ad8adf7a9c9dc7fbd5f4bd1))

## [3.1.3](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.2...v3.1.3) (2022-03-09)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.21 ([17a12a5](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/17a12a5155e8347d6ee092d335c2eaa7088ada8a))
* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.4 ([00ef7bb](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/00ef7bb241f1c06b80afbac6b98d5455b3d09830))

## [3.1.2](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.1...v3.1.2) (2022-02-21)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.19 ([d50997c](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/d50997cda5fe4a9419286f60968efb6067b81659))

## [3.1.1](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.1.0...v3.1.1) (2022-02-21)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.6.3 ([458277b](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/458277b9caab82177032668fab6436f683f81f29))

# [3.1.0](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.0.1...v3.1.0) (2022-01-26)


### Features

* 🎸 Sync reuse of refresh tokens setting ([a10ddd4](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/a10ddd4740384ca1d32d47ddb71baef087fed862))

## [3.0.1](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v3.0.0...v3.0.1) (2022-01-24)


### Bug Fixes

* 🐛 End prematurely if Peurn has thrown an exception ([2607a5e](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/2607a5e1af167da1e6648a59161e2ba9d386a190))

# [3.0.0](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v2.1.1...v3.0.0) (2022-01-19)


### Features

* 🎸 By default rotate refresh tokens ([8dcab3b](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/8dcab3badf8ff84912299e64c4ea6d144a098cf1))


### BREAKING CHANGES

* 🧨 For some RPs, this might be breaking, if they rely on reusing the
refresh token.

## [2.1.1](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v2.1.0...v2.1.1) (2021-11-22)


### Bug Fixes

* 🐛 Limit pool size to 1 connection only ([42680ed](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/42680ed4be78093007d635b5a30e61083627b992))

# [2.1.0](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v2.0.2...v2.1.0) (2021-11-22)


### Features

* 🎸 Enable PostgreSQL ([92d6cc6](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/92d6cc6818de6caae2dfb2696c46074f9a32f5db))

## [2.0.2](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v2.0.1...v2.0.2) (2021-10-14)


### Bug Fixes

* 🐛 Fix response types ([#23](https://github.com/CESNET/spreg_oidc_metadata_sync/issues/23)) ([8b006ac](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/8b006ac2b238ba52c14a7ad12a72ee13971ddb0c))

## [2.0.1](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v2.0.0...v2.0.1) (2021-10-14)


### Bug Fixes

* 🐛 wrong order in hybrid full response ([#22](https://github.com/CESNET/spreg_oidc_metadata_sync/issues/22)) ([186206f](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/186206f02845638ed2658ea56d21fc764624439f))

# [2.0.0](https://github.com/CESNET/spreg_oidc_metadata_sync/compare/v1.0.0...v2.0.0) (2021-10-05)


### Features

* 🎸 Token expiration based on grant type ([#21](https://github.com/CESNET/spreg_oidc_metadata_sync/issues/21)) ([9f99d5f](https://github.com/CESNET/spreg_oidc_metadata_sync/commit/9f99d5ff736a2642f85f140e8c67f2e481fcc55f))


### BREAKING CHANGES

* 🧨 3 configuration options removed (access_token_timeout, id_token_timeout,
refresh_token_timeout), added new configuration option replacing them
(tokens)

## [v1.0.0]
- Initial release with automated semantic release

## [v0.2.1]
- Fix setting grant types

## [v0.2.0]
- Instead of grant types, sync "flowTypes"
- Removed syncing of response types, these are generated based on flow type
- Corrections in the README and other docs

## [v0.1.0]
- First release

[v1.0.0]: https://github.com/CESNET/spreg_oidc_metadata_sync/tree/v1.0.0
[v1.0.0]: https://github.com/CESNET/spreg_oidc_metadata_sync/tree/v0.2.1
[v0.2.0]: https://github.com/CESNET/spreg_oidc_metadata_sync/tree/v0.2.0
[v0.1.0]: https://github.com/CESNET/spreg_oidc_metadata_sync/tree/v0.1.0
